package net.vidux;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class StringUtilTest {
		
	String readUserResult = "{\"login\":\"vertx\",\"id\":687089,\"node_id\":\"MDQ6VXNlcjY4NzA4OQ==\",\"avatar_url\":\"https://avatars1.githubusercontent.com/u/687089?v=4\",\"gravatar_id\":\"\",\"url\":\"https://api.github.com/users/vertx\",\"html_url\":\"https://github.com/vertx\",\"followers_url\":\"https://api.github.com/users/vertx/followers\",\"following_url\":\"https://api.github.com/users/vertx/following{/other_user}\",\"gists_url\":\"https://api.github.com/users/vertx/gists{/gist_id}\",\"starred_url\":\"https://api.github.com/users/vertx/starred{/owner}{/repo}\",\"subscriptions_url\":\"https://api.github.com/users/vertx/subscriptions\",\"organizations_url\":\"https://api.github.com/users/vertx/orgs\",\"repos_url\":\"https://api.github.com/users/vertx/repos\",\"events_url\":\"https://api.github.com/users/vertx/events{/privacy}\",\"received_events_url\":\"https://api.github.com/users/vertx/received_events\",\"type\":\"User\",\"site_admin\":false,\"name\":null,\"company\":null,\"blog\":\"\",\"location\":null,\"email\":null,\"hireable\":null,\"bio\":null,\"public_repos\":1,\"public_gists\":0,\"followers\":2,\"following\":0,\"created_at\":\"2011-03-23T22:07:15Z\",\"updated_at\":\"2014-09-12T17:51:54Z\"}";

	@Test
	public void test_stringUtils() {
		assertEquals("vertx", StringUtils.jsonField(readUserResult, "login").get());
		assertEquals("MDQ6VXNlcjY4NzA4OQ==",StringUtils.jsonField(readUserResult, "node_id").get());		
		assertEquals(false ,StringUtils.jsonField(readUserResult, "").isPresent());
	}

}
