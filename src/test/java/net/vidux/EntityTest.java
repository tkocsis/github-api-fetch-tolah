package net.vidux;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EntityTest {
	Issues issues;
	Issue issue;
	User user;
	Users users;
	Entity entity;
	Entities<Entity> entitys;

	@BeforeEach
	public void setup() {
		entitys= new Entities<Entity>();
		users = new Users();
		User user = new User();
		user.setNodeId("NodeId");
		user.setId(123);
		user.setReposUrl("valami");
		user.setType(Type.ADMIN);
		user.setUrl("valami");
		user.setName("Valaminev");
		users.add(user);
		
		issues= new Issues();
		Issue issue = new Issue();
		issue.setId(1);
		issue.setNodeId("Node");
		issue.setNumber(123);
		issue.setState("state");
		issue.setTitle("title");
		issue.setUser(user);
		issues.add(issue);
		
	}
	@Test
	public void test_entity() {
		assertEquals(new ArrayList<Entity>(), entitys.search("^V"));
		assertEquals(1, issues.search("^t").size());
		assertEquals(0, issues.search("Test").size());
		
	}
	@Test
	public void test2_entity() {
		Issue issue = new Issue();
		issue.setId(1);
		issue.setNodeId("Node");
		issue.setNumber(123);
		issue.setState("state");
		issue.setTitle("hiba");
		issue.setUser(user);
		entitys.add(issue);
		assertEquals(1, entitys.search("^h").size());
		assertEquals("hiba", entitys.search("^h").get(0).getName());
		
	}
	@Test
	public void test3_entity() {
		User user = new User();
		user.setNodeId("NodeId");
		user.setId(123);
		user.setReposUrl("valami");
		user.setType(Type.ADMIN);
		user.setUrl("valami");
		user.setName("Valaki");
		entitys.add(user);		
		assertEquals(1, users.search("^V").size());
		assertEquals("Valaki", entitys.search("V").get(0).getName());
		
		
	}
	@Test
	public void test4_entity() {
		User user2 = new User();
		user2.setNodeId("NodeId");
		user2.setId(123);
		user2.setReposUrl("valami");
		user2.setType(Type.ADMIN);
		user2.setUrl("valami");
		user2.setName("Tamas");
		entitys.add(user2);
		entitys.remove(user2);		
		assertEquals(0, entitys.search("Tamas").size());
	
	}
	
	
}
