package net.vidux;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UsersTest {
	Users users;

	@BeforeEach
	public void setup() {
		users = new Users();
		User user = new User();
		user.setNodeId("NodeId");
		user.setId(123);
		user.setReposUrl("valami");
		user.setType(Type.ADMIN);
		user.setUrl("valami");
		user.setName("Valaminev");
		users.add(user);
		
		User user2 = new User();
		user2.setNodeId("NodeId");
		user2.setId(123);
		user2.setReposUrl("valami");
		user2.setType(Type.ADMIN);
		user2.setUrl("valami");
		user2.setName("Tamas");
		users.add(user2);
	}

	@Test
	public void test_users() {
		assertEquals(new ArrayList<User>(), users.search(null));
		assertEquals(1, users.search("^Va").size());
		assertEquals("Valaminev", users.search("Va").get(0).getName());
		assertEquals(0, users.search("Test").size());

	}

	@Test
	public void test2_user() {
		User user = new User();
		user.setNodeId("NodeId");
		user.setId(123);
		user.setReposUrl("valami");
		user.setType(Type.ADMIN);
		user.setUrl("valami");
		user.setName("Valaki");
		users.add(user);		
		assertEquals(2, users.search("^V").size());
		assertEquals("Valaminev", users.search("V").get(0).getName());
		assertEquals("Valaki", users.search("V").get(1).getName());
		
	}
	@Test
	public void test3_user() {
		User user2 = new User();
		user2.setNodeId("NodeId");
		user2.setId(123);
		user2.setReposUrl("valami");
		user2.setType(Type.ADMIN);
		user2.setUrl("valami");
		user2.setName("Tamas");
		users.add(user2);
		users.remove(user2);
	
	}
	
	
	
	
	
	
}
