package net.vidux;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IssuesTest {
	Issues issues;
	Issue issue;
	User user;
	
	
	@BeforeEach
	public void setup() {
		issues= new Issues();
		Issue issue = new Issue();
		issue.setId(1);
		issue.setNodeId("Node");
		issue.setNumber(123);
		issue.setState("state");
		issue.setTitle("title");
		issue.setUser(user);
		issues.add(issue);
	}
	
	@Test
	public void test_issues() {
		
		assertEquals(new ArrayList<Issue>(), issues.search(null));
		assertEquals(1, issues.search("^t").size());
		assertEquals(0, issues.search("Test").size());
	}
	@Test
	public void test2_issues() {
		Issue issue = new Issue();
		issue.setId(1);
		issue.setNodeId("Node");
		issue.setNumber(123);
		issue.setState("state");
		issue.setTitle("hiba");
		issue.setUser(user);
		issues.add(issue);
		assertEquals(1, issues.search("^h").size());
		assertEquals("hiba", issues.search("^h").get(0).getTitle());
		
	}

	@Test
	public void test3_issues() {
		Issue issue = new Issue();
		issue.setId(1);
		issue.setNodeId("Node");
		issue.setNumber(123);
		issue.setState("state");
		issue.setTitle("hiba");
		issue.setUser(user);
		issues.add(issue);
		issues.remove(issue);
		assertEquals(0, issues.search("^h").size());

	}
	

}
