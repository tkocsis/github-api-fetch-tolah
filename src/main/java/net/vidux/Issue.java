package net.vidux;

public class Issue implements Entity{

	int id;
	String nodeId;
	int number;
	String title;
	User user;
	String state;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "Issue [id=" + id + ", nodeId=" + nodeId + ", number=" + number + ", title=" + title + ", user=" + user
				+ ", state=" + state + "]";
	}
	@Override
	public String getName() {
		return title;
	}
}
