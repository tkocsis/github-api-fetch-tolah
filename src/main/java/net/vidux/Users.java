
package net.vidux;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Users {
	List<User> db = new ArrayList<>();

	public void add(User user) {
		db.add(user);

	}

	public void remove(User user) {

		db.remove(user);
	}

	public List<User> search(String regex) {
		if(regex == null) {
			
			System.err.println("Ez igy nem jo, nincs ertek beadva keresobe(user)");
			return new ArrayList<User>();
		}
		Pattern r = Pattern.compile(regex);
		List<User> s = new ArrayList<>();
		for (User us : db) {
			Matcher m = r.matcher(us.getName());
			if (m.find()) {
				s.add(us);
			}

		}
		return s;
	}

}
