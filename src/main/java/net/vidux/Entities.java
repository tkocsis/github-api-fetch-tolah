package net.vidux;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class   Entities<E extends Entity> {

	List<E> en = new ArrayList<>();
	
	
	public void add(E entity) {
		en.add(entity);
	}
	
	public void remove(E entity) {
		en.remove(entity);
	}
//	public List<E> search(String reg){
//		if (reg == null) {
//			System.err.println("Ez igy nem jo, nincs ertek beadva keresobe(entity)");
//			return new ArrayList<E>();
//		}
//		Pattern r = Pattern.compile(reg);
//		List<E> result = new ArrayList<>();
//		for (E entity : en) {
//			Matcher n = r.matcher(entity.getName());
//			if (n.find()) {
//				result.add(entity);
//			}
//		}
//		return result;
//	}
	
	
	public List<E> search(String regex) {
		if (regex == null) {
			System.err.println("Ez igy nem jo, nincs ertek beadva keresobe(entity)");
			return new ArrayList<E>();
		}		
		Pattern r = Pattern.compile(regex);
		List<E> result = en.stream()
			.filter(re -> {
				Matcher n = r.matcher(re.getName());
				return n.find();
			})
			.collect(Collectors.toList());
		return result;
	}
	
	
//	public List<E> test() {
//		System.out.println("hello");
//		List<E> result = en.stream()
//	
//			.filter(i ->{
//				return i.getId() == 1;
//			})
//	//		.forEach(i -> System.out.println(i));
//			.collect(Collectors.toList());
//		return result;
//	}
//	
	
	
}
