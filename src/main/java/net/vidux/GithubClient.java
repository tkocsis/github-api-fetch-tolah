package net.vidux;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class GithubClient {
	private static GithubClient instance = new GithubClient();

	private GithubClient() {
	}

	public static GithubClient getInstance() {
		return instance;
	}

	public String readUser(String user) {
		String str = "";
		try {
			URL url = new URL("https://api.github.com/users/" + user);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			String line;
			while ((line = br.readLine()) != null) {
				str += line;

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return str;

	}

	public String readRepo(String user) {
		String str = "";
		try {
			URL url1 = new URL("https://api.github.com/users/" + user + "/repos");
			BufferedReader bre = new BufferedReader(new InputStreamReader(url1.openStream()));
			String line1;
			while ((line1 = bre.readLine()) != null) {
				str += line1;

			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return str;

	}

}