package net.vidux;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

public class Main {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {

		System.out.println("Hello");
		User user = new User();
		user.setNodeId("NodeId");
		user.setId(123);
		user.setReposUrl("valami");
		user.setType(Type.ADMIN);
		user.setUrl("valami");
		user.setName("Valaminev");

		Issue issue = new Issue();
		issue.setId(1);
		issue.setNodeId("Node");
		issue.setNumber(123);
		issue.setState("state");
		issue.setTitle("title");
		issue.setUser(user);

		System.out.println(user);

		System.out.println(issue);

		Entities<User> users = new Entities<>();
		users.add(user);
	
		// users.remove(user);
		List<User> result = users.search(null);
		System.out.println("eredmeny:   " + result);

		Entities<Issue> issues = new Entities<>();
		issues.add(issue);
		// issues.remove(issues);
		List<Issue> result1 = issues.search("^t");
		System.out.println("eredmeny issue:  " + result1);

	
		Entities<Entity> entitys = new Entities<>();
		entitys.add(user);
		entitys.add(issue);
		List<Entity> result2 = entitys.search("^V");
		System.out.println("eredmeny entity:  " + result2);
		// entitys.remove(entitys);

		
		GithubClient client = GithubClient.getInstance();
		String readUserResult=client.readUser("vertx");
		System.out.println(readUserResult);
		client.readRepo("vertx");
		
		Optional<String> nodeId = StringUtils.jsonField(readUserResult, "node_id");
		Optional<String> login = StringUtils.jsonField(readUserResult, "login");
		
		System.out.println(login.get());
		System.out.println(nodeId.get());
		User userjson = StringUtils.parseJsonOrg(readUserResult);
		System.out.println(userjson);
		
		User userjackson= StringUtils.parseJackson(readUserResult);
		System.out.println(userjackson);
		
		
	}
}
