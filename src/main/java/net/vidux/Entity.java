package net.vidux;

public interface Entity {

	int getId();

	String getName();

}
