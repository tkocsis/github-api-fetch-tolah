package net.vidux;

import java.io.IOException;
import java.util.Optional;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

public class StringUtils {
	static ObjectMapper mapper = new ObjectMapper();

	public static Optional<String> jsonField(String readUser, String login) {
		String read = readUser.substring(1, readUser.length() - 1);
		String[] comma = read.split(",");

		for (String string : comma) {
			String[] colon = string.split(":");
			if (colon[0].substring(1, colon[0].length() - 1).equals(login)) {
				String center = (colon[1].substring(1, colon[1].length() - 1));
				return Optional.of(center);
			}
		}
		return Optional.empty();
	}

	public static User parseJsonOrg(String userJson) {

		JSONObject jsonObject = new JSONObject(userJson);

		User user = new User();
		user.setName(jsonObject.getString("login"));
		user.setNodeId(jsonObject.getString("node_id"));
		user.setId(jsonObject.getInt("id"));
		user.setReposUrl(jsonObject.getString("repos_url"));
		
		String type = jsonObject.getString("type");
		if (type.equals("User")) {
			user.setType(Type.USER);
		} else if (type.equals("Admin")) {
			user.setType(Type.ADMIN);
		}
		user.setUrl(jsonObject.getString("url"));

		return user;

	}


	
	public static User parseJackson(String userJackson) throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(userJackson, User.class);
	}
}
