package net.vidux;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Issues {
	List<Issue> dd = new ArrayList<>();

	public void add(Issue issue) {
		dd.add(issue);
	}

	public void remove(Issue issue) {

		dd.remove(issue);
	}

	public List<Issue> search(String regexissue) {
		if (regexissue == null) {
			System.err.println("Ez igy nem jo, nincs ertek beadva keresobe(issue)");
			return new ArrayList<Issue>();
		}
		Pattern r = Pattern.compile(regexissue);
		List<Issue> t = new ArrayList<>();
		for (Issue issue2 : dd) {
			Matcher n = r.matcher(issue2.getTitle());
			if (n.find()) {
				t.add(issue2);

			}

		}

		return t;
	}
}
