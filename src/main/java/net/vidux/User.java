package net.vidux;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Entity {

	private int id = 0;

	@JsonProperty("login")
	private String name;
	@JsonProperty("node_id")
	private String nodeId;
	private String url;
	@JsonProperty("repos_url")
	private String reposUrl;
	@JsonIgnore
	private Type type;

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getReposUrl() {
		return reposUrl;
	}

	public void setReposUrl(String reposUrl) {
		this.reposUrl = reposUrl;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", nodeId=" + nodeId + ", url=" + url + ", reposUrl=" + reposUrl
				+ ", type=" + type + "]";
	}

}
